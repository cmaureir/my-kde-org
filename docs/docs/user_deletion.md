# Account deletion in Blender ID

Account deletion can be requested in [Blender ID profile page](https://id.blender.org/):

![Danger zone]()

There are a few edge-cases in which account deletion is not possible:

 * the account is a staff account;
 * there's an active paid subscription linked to the account.

![Staff](images/user_deletion_danger_zone.png)

![Subscription is active](images/user_deletion_subscription.png)

If account deletion is possible, the user is presented with a confirmation form, where the process is explained.

![Delete Account Confirmation Form](images/user_deletion_confirmation.png)

Immediately after deletion is confirmed, the following happens in Blender ID's back-end:

 * all OAuth tokens linked to the account are revoked;
 * the date and time of deletion request are stored in the DB;
 * a `user-modified` event containing this date is sent to all applications that have a webhook configured;
 * the account is deactivated and logged out of Blender ID;
 * a confirmation is displayed:
 
![We are sad to see you go](images/user_deletion_done.png)

## What about connected applications?

It's possible that the account will stay logged in in some of the connected applications, because how the active sessions are handled is up to each individual application.
However it will no longer be possible to **login anew** into any of the applications using Blender ID login.

Blender ID is an OAuth server, and it has no direct control over how applications, connected to it, store and delete their account data.

Applications subscribing to the webhook handle this event at their own discretion.
Deleting Blender ID ≠ deleting all account data in all connected applications.

E.g. consider 3d parties that are out of our control:

- Blenderartists
- Blendercommunity

and the rest of the 24 applications currently registered in [Blender ID](https://id.blender.org/admin/bid_main/oauth2application/).

We'll be able to automate account deletion in *some* of them, starting with Blender Cloud, but definitely not most of them.

## Anonymisation

The account data will be anonymised after the grace period (**14 days** at present) is over.
After the grace period is over, the following happens:

 * the account data stored in Blender ID's database is anonymised;
 * the avatar and any IP addresses stored along this account are deleted.
 
This is handled by the `process_deletion_requests` management command, which periodically called by cron, e.g.:

```
34   2 * * *  root docker exec --user uwsgi blender-id /manage.sh process_deletion_requests
```
