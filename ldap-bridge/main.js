/**
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-LicenseRef: AGPL-3.0-or-later
 */
const ldap = require('ldapjs');
const mysql = require('mysql');
const { spawnSync } = require('child_process');

const server = ldap.createServer();

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'blender_id'
});

server.listen(1389, function() {
  console.log('MyKDE LDAP server up at: %s', server.url);
});

function authorize(req, res, next) {
  /*if (!req.connection.ldap.bindDN.equals('cn=root')) {
    return next(new ldap.InsufficientAccessRightsError());
  }*/

  return next();
}

function loadUsers(req, res, next) {
  connection.connect();
  req.users = {};
 
  connection.query(`SELECT
      bid_main_user.uuid,
      bid_main_user.nickname,
      bid_main_user.first_name,
      bid_main_user.last_name,
      bid_main_user.email,
      bid_main_role.name AS role
    FROM
      bid_main_user
    INNER JOIN
      bid_main_user_roles
    INNER JOIN
      bid_main_role
    WHERE
      bid_main_user_roles.user_id = bid_main_user.id
    AND
      bid_main_user_roles.role_id = bid_main_role.id;`, function (error, results, fields) {
    if (error) throw error;
    for (let result of results) {
      if (req.users[result.full_name]) {
        // just add the role
        req.users[result.full_name].attributes.groupMember.push(result.role);
      } else {
        req.users[result.full_name] = {
          dn: 'uid=' + result.nickname + ', ou=people, dc=kde, dc=org',
          attributes: {
            cn: result.full_name,
            sn: result.last_name,
            givenName: result.first_name,
            uid: result.uuid,
            mail: result.email,
            objectclass: ['top', 'person', 'organizationalPerson'],
            groupMember: [result.role],
          }
        };
      }
    }
    return next();
  });
   
  connection.end();
}

let pre = [authorize, loadUsers];

server.bind('cn=root', function(req, res, next) {
  if (req.dn.toString() !== 'cn=root' || req.credentials !== 'secret')
    return next(new ldap.InvalidCredentialsError());

  res.end();
  return next();
});

server.bind('', function(req, res, next) {
  const dns = {};
  req.dn.toString().split(',')
    .forEach((dn) => {
      const t = dn.split('=');
      dns[t[0]] = t[1];
    });

  let opts = [];

  // TODO sanitize input 
  if (dns['uid'])  {
    opts = ['authentificate', '--username=' + dns['uid'], '--password=' + req.credentials]
  } else if (dns['cn']) {
    opts = ['authentificate', '--username=' + dns['cn'], '--password=' + req.credentials]
  }
  const djangoAuthorize = spawnSync('../manage.py', opts);

  if (djangoAuthorize.status === 0) {
    res.end();
    return next();
  }
  return next(new ldap.InvalidCredentialsError());
});

server.search('o=myhost', pre, function(req, res, next) {
  Object.keys(req.users).forEach(function(k) {
    if (req.filter.matches(req.users[k].attributes))
      res.send(req.users[k]);
  });

  res.end();
  return next();
});
