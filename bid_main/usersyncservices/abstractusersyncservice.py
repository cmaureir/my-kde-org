from .. import models


class AbstractUserSyncService:
    def __init__(self, url: str, name: str):
        """
        Initialize a service
        Args:
            url: The url of the service (e.g. https://invent.kde.org)
            name: The name of the service (e.g. Invent, Userbase)
        """
        self.url = url
        self.name = name

    def user_exist() -> bool:
        """This method should test if the user exist on the service. If the user
        doesn't exist this method will do nothing."""
        return False

    def update_profile(self, user: models.User) -> None:
        """This method should be responsible for updating the user profile on the
        service and can assume the user exist."""
        pass
