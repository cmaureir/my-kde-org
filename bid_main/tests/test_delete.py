"""Tests for the Delete account section of the profile."""
from datetime import timedelta
from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

import oauth2_provider.models as oa2_models

Application = oa2_models.get_application_model()
AccessToken = oa2_models.get_access_token_model()
RefreshToken = oa2_models.get_refresh_token_model()
UserModel = get_user_model()


class DeleteTest(TestCase):
    def setUp(self):
        self.application = Application.objects.create(
            name="test_client_credentials_app",
            user=None,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_CLIENT_CREDENTIALS,
        )
        self.user = UserModel.objects.create_user("test@user.com", "123456")

    def test_login_required(self):
        response = self.client.post(
            reverse("bid_main:delete_user"),
            {
                "confirm": True,
            }
        )
        self.assertEqual(302, response.status_code, f"response: {response}")

    def test_invalid_form(self):
        self.assertTrue(self.user.is_active)
        self.assertIsNone(self.user.date_deletion_requested)
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("bid_main:delete_user"),
            {
                "confirm": False,
            }
        )
        self.assertEqual(200, response.status_code, f"response: {response}")
        self.assertNotIn("We are sad to see you go", str(response.content))
        # No change
        self.user.refresh_from_db()
        self.assertTrue(self.user.is_active)
        self.assertIsNone(self.user.date_deletion_requested)

    def test_invalid_form_missing(self):
        self.assertTrue(self.user.is_active)
        self.assertIsNone(self.user.date_deletion_requested)

        self.client.force_login(self.user)
        response = self.client.post(
            reverse("bid_main:delete_user"),
        )
        self.assertEqual(200, response.status_code, f"response: {response}")
        self.assertNotIn("We are sad to see you go", str(response.content))
        # No change
        self.user.refresh_from_db()
        self.assertTrue(self.user.is_active)
        self.assertIsNone(self.user.date_deletion_requested)

    def test_deactivates_user_stores_date_and_revokes_user_tokens(self):
        self.assertTrue(self.user.is_active)
        self.assertIsNone(self.user.date_deletion_requested)
        access_token = AccessToken.objects.create(
            user=self.user,
            scope="email badge",
            expires=timezone.now() + timedelta(days=300),
            token="asdfasdfasdfasdfasdfasd",
            application=self.application,
        )
        refresh_token = RefreshToken.objects.create(
            user=self.user,
            application=self.application,
            revoked=None,
        )
        another_user = UserModel.objects.create_user(
            "test1@user.com", "1234567", nickname="nickname"
        )
        anothers_access_token = AccessToken.objects.create(
            user=another_user,
            scope="email badge",
            expires=timezone.now() + timedelta(days=300),
            token="qakljhlkasjdflakjsdhfalkjsdhljkasd",
            application=self.application,
        )
        anothers_refresh_token = RefreshToken.objects.create(
            user=another_user,
            application=self.application,
            revoked=None,
        )

        self.client.force_login(self.user)
        response = self.client.post(
            reverse("bid_main:delete_user"),
            {
                "confirm": True,
            }
        )
        self.assertEqual(200, response.status_code, f"response: {response}")
        self.assertIn('We are sad to see you go', str(response.content))
        self.user.refresh_from_db()
        self.assertFalse(self.user.is_active)
        self.assertIsNotNone(self.user.date_deletion_requested)

        refresh_token.refresh_from_db()
        access_token.refresh_from_db()
        self.assertLess(access_token.expires, timezone.now())
        self.assertLess(refresh_token.revoked, timezone.now())

        anothers_refresh_token.refresh_from_db()
        anothers_access_token.refresh_from_db()
        self.assertGreater(anothers_access_token.expires, timezone.now())
        self.assertIsNone(anothers_refresh_token.revoked)

    def test_does_not_override_set_date(self):
        deleted_user = UserModel.objects.create_user(
            "test1@user.com", "1234567", nickname="nickname"
        )
        current_date = timezone.now()
        deleted_user.date_deletion_requested = current_date
        deleted_user.save(update_fields=["date_deletion_requested"])
        self.assertTrue(self.user.is_active)

        self.client.force_login(deleted_user)
        response = self.client.post(
            reverse("bid_main:delete_user"),
            {
                "confirm": True,
            }
        )
        self.assertEqual(200, response.status_code, f"response: {response}")
        self.assertIn("We are sad to see you go", str(response.content))
        deleted_user.refresh_from_db()
        self.assertFalse(deleted_user.is_active)
        # The date wasn't updated
        self.assertEquals(deleted_user.date_deletion_requested, current_date)

    @patch('bid_main.email.send_mail')
    def test_sends_an_email(self, mock_send_mail):
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("bid_main:delete_user"),
            {
                "confirm": True,
            }
        )
        self.assertEqual(200, response.status_code, f"response: {response}")
        self.assertIn('We are sad to see you go', str(response.content))
        mock_send_mail.assert_called_once()
        self.assertEquals(
            mock_send_mail.call_args.args[0],
            'Blender ID account deletion',
        )
        self.assertEquals(
            mock_send_mail.call_args.kwargs['recipient_list'],
            [self.user.email],
        )
