{% autoescape off %}
Hi {{ user.full_name|default:user.email }}!

You are receiving this message because you have requested deletion of your MyKDE account.
Your account has been deactivated and you've been logged out of MyKDE.
Data linked to the account will not be deleted for another 2 weeks.

**Important**: you will not be able to create a new account with the same email address until this process is completed.

We are sad to see you go!

Kind regards,

The KDE Systadmin team
{% endautoescape %}
